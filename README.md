# aka Rehearsal Stage

[Other Geometries >> Rehearsal Stage](https://geometries.xyz/t-interface/type+source.html) is work-in-progress interface for experimenting with examples of code functions (written JavaScript) part of our movement glossary. 
This online and browser-based environment contains a text input field for typing and calling existing 'movement' functions from the glossary — as well as — modify those functions or write new ones. 
